<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 28 - JoseMiguel & JeanClaude</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>

    <div class= "title"><h1> Ejercicio 28 </h1></div>

    <div class= "ejer"> 
        <h2> Ejercicio 28 include_once() </h2>

        <p>Crea un fichero en php que incluya dentro de él una llamada con include_once() al fichero funciones.inc.php creado en el ejercicio 26, comprobando cada una de las funciones.</p>

        <h2> Funciones: </h2>

        <?php



            echo "<h1>Funciones</h1>";

            include_once("funciones.inc.php");


            echo "<h1>Comprobaciones</h1>";

            echo "<strong>23. La media de los números (7,7,8,8): <br></strong>";
            echo "<br>";
            media(7,7,8,8);
            echo "<br>";
            echo "<strong>24. Cuentavocales de la frase: 'Hola me llamo Armando'</strong>";
            echo "<br>";
            cuentavocales("Hola me llamo Armando");
            echo "<br>";
            echo "<strong>25-a. Cuadrado de 8: <br></strong>";
            echo "<br>";
            cuadrado("#",8);
            echo "<br>";
            echo "<strong>25-b. Loteria: <br></strong>";
            echo "<br>";
            loteria(6,60);

            

        ?>

    </div>






    <a href="/PHP-Boletin2/index.html"><img src="images/share.png" width="100px" height="100px"alt="" srcset=""></a>
</body>
</html>