<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 26 - JoseMiguel & JeanClaude</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>

<!-- EJ23 -->

    <div class= "title"><h1> Ejercicio 23 </h1></div>

    <div class= "subejer"> 

        <h2> Ejercicio 23 Función media </h2>

        <p>Hacer una página PHP que contenga una función “media” que reciba 4 números como 
        parámetros y devuelva el valor medio de los parámetros recibidos. </p>

        <?php

            function media($num1, $num2, $num3, $num4){

                // Hacemos la operación para alcanzar la media
                $media = ($num1 + $num2 + $num3 + $num4) / 4;

                // Devolverá el resultado de la operación anterior
                echo "La media es: ". $media;
                echo "<br>";
                
            }


            media(1,1,2,2);

        ?>
    </div>






<!-- EJ24 -->


    <div class= "title"><h1> Ejercicio 24 </h1></div>

    <div class= "subejer"> 
        
        <h2> Ejercicio 24 Función cuentavocales </h2>

        <p>Hacer una página PHP que contenga una función “cuentavocales” que recibe una cadena de 
        texto y muestra por pantalla el número de vocales totales que tiene la cadena</p>


        <?php

        function cuentavocales($cadena_texto){
        
            // Convertimos la cadena para que sea entera en minúsculas
            $cadena_texto = strtolower($cadena_texto);

            // Creamos un array para almacenar cada una de las vocales
            $vocales = array('a' => 0, 'e' => 0, 'i' => 0, 'o' => 0,'u' => 0);

            // Creamos un for para recorrer toda la cadena
            for($j = 0; $j < strlen($cadena_texto); $j++){

                $letra = $cadena_texto[$j];

                // Vamos revisando si cada letra es una vocal
                if(array_key_exists($letra, $vocales)){
                    // Sumamos al contador de vocales
                    $vocales[$letra]++;
                }
        }

        // Total de vocales
        $vocales_total = array_sum($vocales);

        echo "<br> La cadena de texto <strong>'$cadena_texto'</strong> introducida contiene <strong>'$vocales_total' vocales en total. </strong> <br>";

        // Hacemos un for each para poder sacar por pantalla cuantas vocales de cada tipo hay
        foreach ($vocales as $vocal => $contador) {
            echo "Vocal <strong>'$vocal'</strong>: <strong>$contador</strong> veces en total. <br>";
            }
            echo "<br>";

        }


        cuentavocales("hola que tal estas");

        ?>

    </div>


<!-- EJ25 A -->



    <div class= "title"><h1> Ejercicio 25 </h1></div>

    <div class= "subejer"> 
        
        <h2> Ejercicio 25 Función cuadrado </h2>

        <p>Hacer una página PHP que contenga una función “cuadrado” que recibe 2 parámetros, un carácter (que puede ser cualquiera) y un número. La función debe mostrar por pantalla un cuadrado con el carácter recibido (tantas filas y columnas como indique el número)  </p>


        <?php


        function cuadrado($caracter,$numero){
            for($i=0;$i<$numero;$i++){
                for($j=0;$j<$numero;$j++){
                    echo $caracter."";
                }
                echo "<br>";
                

            }
            echo "<br>";

        }


        cuadrado("#",5);

        ?>

    </div>



<!-- EJ25 - B -->

    <div class= "title"><h1> Ejercicio 25 - B </h1></div>

    <div class= "subejer"> 
        <h2> Ejercicio 25 Función loteria </h2>

        <p>Realiza una página PHP que contenga una función “lotería” que reciba 2 parámetros, un número que indique las bolas a extraer y un segundo número que indique cuantas bolas hay en el bombo. La función debe generar los números aleatorios y mostrarlos por pantalla según los parámetros introducidos. </p>

        <?php

        function loteria($numeroBolasAextraer,$bolasQueHayEnElBombo){
            $numerosElegidos=array();
            for($i=0;$i<$numeroBolasAextraer;$i++){
                do{
                    $resultado=rand(1,$bolasQueHayEnElBombo);
                }while(in_array($resultado,$numerosElegidos));
                $numerosElegidos[]=$resultado;

                echo $resultado." ";

            }
        }

        loteria(7,80);

        ?>
    </div>






    

</body>
</html>