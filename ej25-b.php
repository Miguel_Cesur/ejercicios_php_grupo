<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 25-b - JoseMiguel & JeanClaude</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class= "title"><h1> Ejercicio 25 - B </h1></div>

    <div class= "ejer"> 
            
        <h2> Ejercicio 25 Función loteria </h2>

        <p>Realiza una página PHP que contenga una función “lotería” que reciba 2 parámetros, un número que indique las bolas a extraer y un segundo número que indique cuantas bolas hay en el bombo. La función debe generar los números aleatorios y mostrarlos por pantalla según los parámetros introducidos. </p>
        
        <?php

            function loteria($numeroBolasAextraer,$bolasQueHayEnElBombo){
                $numerosElegidos=array();
                for($i=0;$i<$numeroBolasAextraer;$i++){
                    do{
                        $resultado=rand(1,$bolasQueHayEnElBombo);
                    }while(in_array($resultado,$numerosElegidos));
                    $numerosElegidos[]=$resultado;

                    echo $resultado." ";
                }
            }

                loteria(6,49);

        ?>
    </div>



    <code>

    <h2>FUNCION</h2>

    function loteria($numeroBolasAextraer,$bolasQueHayEnElBombo){<br>
                $numerosElegidos=array();<br>
                for($i=0;$i<$numeroBolasAextraer;$i++){<br>
                    do{<br>
                        $resultado=rand(1,$bolasQueHayEnElBombo);<br>
                    }while(in_array($resultado,$numerosElegidos));<br>
                    $numerosElegidos[]=$resultado;<br>

                    echo $resultado." ";<br>
                }<br>
            }<br>












    </code>
























    <a href="/PHP-Boletin2/index.html"><img src="images/share.png" width="100px" height="100px"alt="" srcset=""></a>




    

</body>
</html>