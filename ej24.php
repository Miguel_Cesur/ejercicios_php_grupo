<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 24 - JoseMiguel & JeanClaude</title>
    <link rel='stylesheet' href="style.css">
</head>

<body>

    <div class= "title"><h1> Ejercicio 24 </h1></div>

    <div class= "ejer"> 
        
        <h2> Ejercicio 24 Función cuentavocales </h2>

        <p>Hacer una página PHP que contenga una función “cuentavocales” que recibe una cadena de 
        texto y muestra por pantalla el número de vocales totales que tiene la cadena</p>



        <?php

            $cadena_texto = "Hola me llamo Miguel";

            echo "<br> Vamos a sacar las vocales de la siguiente cadena de texto: <br>";

            echo "<br><strong> '$cadena_texto' </strong><br>";

            function cuentavocales($cadena_texto){
            
                // Convertimos la cadena para que sea entera en minúsculas
                $cadena_texto = strtolower($cadena_texto);

                // Creamos un array para almacenar cada una de las vocales
                $vocales = array('a' => 0, 'e' => 0, 'i' => 0, 'o' => 0,'u' => 0);

                // Creamos un for para recorrer toda la cadena
                for($j = 0; $j < strlen($cadena_texto); $j++){

                    $letra = $cadena_texto[$j];

                    // Vamos revisando si cada letra es una vocal
                    if(array_key_exists($letra, $vocales)){
                        // Sumamos al contador de vocales
                        $vocales[$letra]++;
                    }
            }

            // Total de vocales
            $vocales_total = array_sum($vocales);

            echo "<br> La cadena de texto <strong>'$cadena_texto'</strong> introducida contiene <strong>'$vocales_total' vocales en total. </strong> <br>";
            

            // Hacemos un for each para poder sacar por pantalla cuantas vocales de cada tipo hay
            foreach ($vocales as $vocal => $contador) {
                echo "Vocal <strong>'$vocal'</strong>: <strong>$contador</strong> veces en total. <br>";
                }

            }

            cuentavocales($cadena_texto);

        ?>

    </div>



        
        <code >

            <h2>FUNCION</h2>

            function cuentavocales($cadena_texto)   { <br>
            
            // Convertimos la cadena para que sea entera en minúsculas<br>
            $cadena_texto = strtolower($cadena_texto);<br>

            // Creamos un array para almacenar cada una de las vocales<br>
            $vocales = array('a' => 0, 'e' => 0, 'i' => 0, 'o' => 0,'u' => 0);<br>

            // Creamos un for para recorrer toda la cadena<br>
            for($j = 0; $j < strlen($cadena_texto); $j++){<br>

                $letra = $cadena_texto[$j];<br>

                // Vamos revisando si cada letra es una vocal<br>
                if(array_key_exists($letra, $vocales)){<br>
                    // Sumamos al contador de vocales<br>
                    $vocales[$letra]++;<br>
                }
            }
        }





        </code>
    



   

        <a href="/PHP-Boletin2/index.html"><img src="images/share.png" width="100px" height="100px"alt="" srcset=""></a>





</body>
</html>

