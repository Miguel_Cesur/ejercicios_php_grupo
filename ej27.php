<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 27 - JoseMiguel & JeanClaude</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>

    <div class= "title"><h1> Ejercicio 27 </h1></div>

    <div class= "ejer"> 
        <h2> Ejercicio 27 include() </h2>

        <p>Crea un fichero en php que incluya dentro de él una llamada con include() al fichero funciones.inc.php creado en el ejercicio 26, comprobando cada una de las funciones. </p>

        <?php
        

            echo "<h1>Funciones</h1>";

        
            include("funciones.inc.php");  

            echo "<h1>Comprobaciones</h1>";

            echo "<strong>23. La media de los números (2,2,3,3): <br></strong>";
            echo "<br>";
            media(2,2,3,3);
            echo "<br>";
            echo "<strong>24. Cuentavocales de la frase: 'Hola me llamo Jean Claude'</strong>";
            echo "<br>";
            cuentavocales("Hola me llamo Jean Claude");
            echo "<br>";
            echo "<strong>25-a. Cuadrado de 4: <br></strong>";
            echo "<br>";
            cuadrado("#",4);
            echo "<br>";
            echo "<strong>25-b. Loteria: <br></strong>";
            echo "<br>";
            loteria(6,60);

            
        ?>
    </div>




    <a href="/PHP-Boletin2/index.html"><img src="images/share.png" width="100px" height="100px"alt="" srcset=""></a>
























</body>
</html>